"use strict";
/* global Vue CouchEvents Network Channel Query */


/* exported vm */
var vm = new Vue({
    el: "#app",
    data: {
        networks: {},
        connected: "disconnected",
        selectedNetwork: "",
        selectedChannel: "",
    },
    methods: {
        keys: (obj) => {
            return Object.keys(obj || {});
        },
        values: (obj) => {
            return Object.values(obj || {});
        },
        localeTime: (time) => {
            return (new Date(time)).toLocaleTimeString();
        },
    },
});


/* exported Couch */
var Couch = (function () {
    const PING_INTERVAL = 20 * 1000;
    const PONG_TIMEOUT = 2 * 1000;
    var socket, pingTimeout, pingsLost = 0;


    // Manage ping timeout-ing
    setInterval(sendPing, PING_INTERVAL);

    function sendPing() {
        sendSocketMessage("ping");

        pingTimeout = setTimeout(function () {
            console.log("Ping timeout detected.");
            pingsLost += 1;
            CouchEvents.emit("pingTimeout");

            disconnect();
            if (pingsLost <= 10) {
                CouchEvents.emit("reconnecting");
                connect();
            }
            else {
                console.log("Giving up reconnecting");
            }

        }, PONG_TIMEOUT);
    }

    CouchEvents.on("pong", () => {
        clearTimeout(pingTimeout);
    });

    CouchEvents.on("ping", () => {
        sendSocketMessage("pong");
    });


    // Socket stuffs
    connect();
    function connect() {
        let protocol = location.protocol == "https:" ? "wss" : "ws",
            host = location.hostname,
            port = location.port ? ":"+location.port : "";

        socket = new WebSocket(`${protocol}://${host}${port}`);

        socket.addEventListener("open", onSocketOpen);
        socket.addEventListener("message", onSocketMessage);
        socket.addEventListener("close", onSocketClose);

        vm.connected = "connected";
        pingsLost = 0;
    }

    function disconnect() {
        socket.close();
    }

    function onSocketOpen() {
        // TODO: figure out what I want to do here
        CouchEvents.emit("socket_open");
    }

    function onSocketClose() {
        // TODO: figure out what I want to do here
        CouchEvents.emit("socket_close");
        vm.connected = "disconnected";
    }

    function onSocketMessage(event) {
        var msg = JSON.parse(event.data);
        CouchEvents.emit(msg.event, msg.data);

        if (msg.event !== "ping" && msg.event !== "pong") { // I get it - you are pinging - but shut up
            console.debug("recv", msg);
        }
    }

    function sendSocketMessage(event, data) {
        if (socket.readyState !== socket.OPEN) {
            console.error(`Cannot send ${event} (${data}), because the socket isn't open`);
            return;
        }

        data = data || {};
        let msg = {event: event, data: data};
        socket.send(JSON.stringify(msg));

        if (msg.event !== "ping" && msg.event !== "pong") { // I get it - you are pinging - but shut up
            console.debug("send", msg);
        }
    }

    CouchEvents.on("server_shutdown", disconnect);


    // All the events

    // Basicly "scrollback" - we get this event on connecting
    CouchEvents.on("network_info", (event) => {
        var data = Object.assign({}, event); // copy

        // Convert the channels into channel objects before adding network
        for (let channel of Object.values(data.channels)) {
            if (channel.type === "channel") {
                channel = new Channel(channel);
            }
            else { // channel.type === "query"
                channel = new Query(channel);
            }
        }

        // Editing object -> need to use vue methods
        vm.$set(vm.networks, data.name, new Network(data));

        // TODO: look into why this is not setting in above
        vm.networks[data.name].lobby = data.lobby;
    });

    CouchEvents.on("message", (event) => {
        vm.networks[event.network].channels[event.channel || event.target].messages.push(event);
    });

    CouchEvents.on("lobby_message", (event) => {
        vm.networks[event.network].lobby.push(event);
    });

    // TODO: Events for join channels, part channels, join networks, leave networks, new query, etc

    // "API"
    return {
        connect: connect,
        disconnect: disconnect,
        sendMessage: sendSocketMessage,
    };
})();
