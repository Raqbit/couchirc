# Documentation for the config

You can find the default config under server/default_config.json.

### bind_port
  The port that Couch will listen on/Couch (client) will be available on in your browser.

### networks
  The networks to join. The key must be a unique name (and identifiable).

  * #### nick
    The nick to join as

  * #### username
    The username to use

  * #### realname 
    The realname to use

  * #### password
    The server's password. If it has none, irc-framework will try to authenticate you   with SASL, using username and password.

  * #### host
    The host to connect to

  * #### port
    The port to connect to. Commonly 6667 (non-ssl) and 6697 (ssl)

  * #### ssl
    Whether to use ssl

  * #### channels
    The channels to autojoin. Key is the channel, and value is the channels "key"   (password). eg. If the channel does not have a key(password) "##CouchIrc": "", and   if it does "#yourchannel": "password".

  * #### debug
    Whether to display raw irc traffic. Requires the "main" debug to be true.

### debug
  The "main" debug. Displays Couch debug messages.

### configVersion
  The version of this config. **DO NOT TOUCH**. It is used to update the config from older versions, and is automagicly modified.