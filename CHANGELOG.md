# Pre v0.2.0
* Did initial work on it
* Got to a 'working demo' stage of development

# v0.2.0
* Rewrote a lot of modules, server and clientside,  to be, well, *modular*
* also changed event seperators to be `.` instead of `::`
* Refined eslint rules a bit more
* dropped planned 'lzwCompress' dependancy, since it - for my few test cases - increased the size of data being sent
* reworked Couch & the configs to not assume only one connection/network => can now connect to multiple networks (though that is untested :<)
