const Message = require(__serverRoot + "models/Message");

function Query(recipient) {
    this.recipient = recipient;
    this.messages = [];

    this.type = "query";
}

Query.prototype.handle = function (eventType, event) {
    // TODO: in the future, this will be parsed/interpreted/whatever, but for now just get this working
    var msg = Object.assign({type: eventType}, event); // Copy
    this.messages.push(new Message(msg));
};

Query.prototype.getInfo = function() {
    var info = Object.assign({}, this);
    info.messages.map(message => {return message.getInfo();});

    return info;
};