function Message (messageData) {
    // TODO: in the future, this will be parsed/interpreted/whatever, but for now just get this working
    Object.assign(this, messageData); // not all messages have a time
    this.time = this.time || (new Date()).getTime();
    this.type = this.type || "unknown";
}

Message.prototype.getInfo = function () {
    return this;
};

module.exports = Message;