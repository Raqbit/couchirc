const Message = require(__serverRoot + "models/Message");
const CouchEvents = require(__serverRoot + "CouchEvents");

function Channel(channel, key, networkName) {
    this.channel = channel;
    this.key = key;
    this.users = [];
    this.modes = [];
    this.createdAt = 0;
    this.url = "";
    this.messages = [];
    this.topic = {};
    this.topicSetBy = {};
    this.networkName = networkName;

    this.type = "channel";
}

Channel.prototype.handle = function(eventType, event) {
    switch (eventType) {
        // Handle channel data
        case "channel_info":
            this.modes = event.modes || this.modes;
            this.createdAt = event.created_at || this.createdAt;
            this.url = event.url || this.url;
            return; // Don't append msgs

        case "topic":
            this.topic = event;
            break;

        case "topicsetby":
            this.topic.nick = event.nick;
            this.topic.user = event.user;
            this.topic.host = event.host;
            this.topic.time = event.time;
            break;

        // Handle userlist
        case "userlist":
        case "wholist": // TODO: look into diffrence?? Maybe merge them?
            {
                let newUsers = event.users;
                for (let newUser of newUsers) {
                    let i = this.users.findIndex(user => user.nick === newUser.nick);
                    if (i === -1) {
                        this.users.push(newUser);
                    }
                }

                CouchEvents.emit("irc.userlist.update", {network: this.networkName, channel:this.channel, userlist: this.users});
            }
            return; // Don't append msgs

        case "join":
            if (!event.modes) {
                event.modes = []; // We don't have a mode key, but others' do
            }
            this.users.push(event);
            CouchEvents.emit("irc.channel_userlist.add", {network: this.networkName, channel:this.channel, user: event});
            break;
        case "part":
        case "kick":
        case "quit": {
            let index = this.users.findIndex(user => user.nick === event.nick);
            if (index === -1) {
                return;
            }
            this.users.splice(index, 1);
            CouchEvents.emit("irc.channel_userlist.remove", {network: this.networkName, channel:this.channel, user: event});
            break;
        }
        case "nickchange": {
            let user = this.users.find(user => user.nick === event.nick);
            if (!user) {
                return;
            }
            user.nick = event.new_nick;
            CouchEvents.emit("irc.channel_userlist.rename", {network: this.networkName, channel:this.channel, userlist: event});
            break;
        }
    }

    // TODO: in the future, this will be parsed/interpreted/whatever, but for now just get this working
    var msg = Object.assign({type: eventType}, event); // Copy
    this.messages.push(new Message(msg));
};

Channel.prototype.getInfo = function() {
    var info = Object.assign({}, this);
    delete info.networkName;
    info.messages.map(message => {return message.getInfo();});

    return info;
};

module.exports = Channel;