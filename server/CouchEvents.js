const EventEmitter = require("events");
const CouchEvents = new EventEmitter();

CouchEvents.on("error", (err) => {
    console.error(err);
});

module.exports = CouchEvents;
