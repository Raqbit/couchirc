const path = require("path");

const ClientManager = require(__serverRoot + "server/ClientManager");
const CouchEvents = require(__serverRoot + "CouchEvents");
const Network = require(__serverRoot + "irc/Network");
const log = require(__serverRoot + "Logger")("Couch");
const WebServer = require(__serverRoot + "./server/WebServer");

var Couch = function() {
    if (DEVMODE) {
        log.info(`Starting Couch v${COUCH_VERSION} in development mode!`);
    }
    else {
        log.info(`Starting Couch v${COUCH_VERSION}`);
    }

    // Load the config
    var CouchConfig = require(__serverRoot + "Config");
    CouchConfig.load(path.resolve(__root + "config.json"));
    var config = CouchConfig.config;

    log.setShowDebug(config.debug);
    if (DEVMODE && !config.debug) {
        log.info("Devmode is active, but debug messages are not set true in the config!");
    }

    // Connect to networks
    var networks = {};
    for (var networkName in config.networks) {
        networkName = networkName.toLocaleLowerCase();
        var settings = Object.assign({name: networkName}, config.networks[networkName]); // copy, so it doesn't get changed by irc-framework
        settings.gecos = settings.realname; // alias
        // eslint-disable-next-line camelcase
        settings.enable_echomessage = true;

        var network = new Network(settings);
        networks[networkName] = network;
        network.connect();
    }

    // Load (websocket) Client Manager
    var clientManager = ClientManager();

    // Load Webserver
    var webserver = WebServer();


    // Send scrollback to ws client
    // TODO: limit this
    CouchEvents.on("client.connected", (client) => {
        for (let network of Object.values(networks)) {
            client.send("network_info", network.getInfo());
        }
    });

    // Send info as we get it (need to watch for changes first)
    CouchEvents.on("irc.network.connected", (network) => {
        network.lobby = new Proxy(network.lobby, {
            set: function (target, property, value, receiver) {
                if (property !== "length") {
                    clientManager.broadcast("lobby_message", Object.assign({network: network.name}, value));
                }

                target[property] = value;
                return true;
            }
        });
    });

    CouchEvents.on("irc.joined_channel", event => {
        let channel = event.channel;
        clientManager.broadcast("join_channel", channel);
        channel.messages = new Proxy(channel.messages, {
            set: function (target, property, value, receiver) {

                if (property !== "length") {
                    clientManager.broadcast("message", Object.assign({network: network.name}, value));
                }

                target[property] = value;
                return true;
            },
        });
    });

    [
        "irc.channel_userlist.add",
        "irc.channel_userlist.remove",
        "irc.channel_userlist.rename",
        "irc.channel_userlist.update",
        "irc.whois_reply",
        "irc.nickchange.inuse",
        "irc.nickchange.invalid",
        "irc.nickchange.success"
    ].forEach(eventName => {
        CouchEvents.on(eventName, event => {
            clientManager.broadcast(eventName.replace(/^irc\./, ""), event);
        });
    });



    // Listen for ws client "commands"
    // TODO: what is security?
    CouchEvents.on("ws.join_channel", (event) => {
        let network = event.data.network,
            channel = event.data.channel,
            key = event.data.key;

        if (!networks[network] || !channel) {
            return;
        }

        networks[network].join(channel, key);
    });

    CouchEvents.on("ws.leave_channel", (event) => {
        let network = event.data.network,
            channel = event.data.channel,
            message = event.data.message;

        if (!networks[network] || !channel) {
            return;
        }

        networks[network].part(channel, message);
    });

    CouchEvents.on("ws.change_nick", (event) => {
        let network = event.data.network,
            newNick = event.data.newNick;

        if (!networks[network]) {
            return;
        }

        networks[network].changeNick(newNick);
    });

    CouchEvents.on("ws.send_message", (event) => {
        let network = event.data.network,
            target = event.data.target,
            message = event.data.message;

        if (!networks[network] || !target) {
            return;
        }

        networks[network].connection.irc.say(target, message);

        // Echo messages to ourselves if the server doesn't support it
        var irc = networks[network].connection.irc;
        if (!irc.network.cap.isEnabled("echo-message")) {
            networks[network].connection.emit("privmsg", {
                nick: irc.user.nick,
                ident: irc.user.username,
                hostname: irc.user.host,
                target: target,
                message: message,
                time: new Date().getTime()
            });
        }
    });

    CouchEvents.on("ws.send_notice", (event) => {
        let network = event.data.network,
            target = event.data.target,
            message = event.data.message;

        if (!networks[network] || !target) {
            return;
        }

        networks[network].connection.irc.notice(target, message);

        // Echo messages to ourselves if the server doesn't support it
        var irc = networks[network].connection.irc;
        if (!irc.network.cap.isEnabled("echo-message")) {
            networks[network].connection.emit("notice", {
                // eslint-disable-next-line camelcase
                from_server: false,
                nick: irc.user.nick,
                ident: irc.user.username,
                hostname: irc.user.host,
                target: target,
                group: "", // TODO: have to implemenet this
                message: message,
                tags: [],
                time: new Date().getTime()
            });
        }
    });

    CouchEvents.on("ws.send_action", (event) => {
        let network = event.data.network,
            target = event.data.target,
            message = event.data.message;

        if (!networks[network] || !target) {
            return;
        }

        networks[network].connection.irc.action(target, message);

        // Echo messages to ourselves if the server doesn't support it
        var irc = networks[network].connection.irc;
        if (!irc.network.cap.isEnabled("echo-message")) {
            networks[network].connection.emit("action", {
                nick: irc.user.nick,
                ident: irc.user.username,
                hostname: irc.user.host,
                target: target,
                message: message,
                tags: [],
                time: new Date().getTime()
            });
        }
    });

    CouchEvents.on("ws.whois", (event) => {
        let network = event.data.network,
            target = event.data.target;

        if (!networks[network] || !target) {
            return;
        }

        networks[network].connection.irc.whois(target);
    });

    // Shutdown stuff
    CouchEvents.on("couch.shutdown", function() {
        Object.values(networks).forEach((connection) => {
            connection.disconnect("Couch shutting down");
        });
    });

    return {
        config: CouchConfig,
        networks: networks,
        clientManager: clientManager,
        webserver: webserver,
    };
};

module.exports = Couch;
