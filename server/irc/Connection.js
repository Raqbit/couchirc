const IRC = require("irc-framework");
const EventEmitter = require("events").EventEmitter;
const logger = require(__serverRoot + "Logger");

class Connection extends EventEmitter {

    constructor(connectionSettings) {
        super();
        this.settings = connectionSettings;
        this.irc = new IRC.Client();
        this.log = logger(`IRCConnection ${connectionSettings.name}`);
        this.registered = false;

        this._init();
    }

    _init() {
        // Emit irc events ourselves (for ease mostly)
        var self = this;
        function EventMiddleware() {
            return function(client, rawEvents, parsedEvents) {
                parsedEvents.use(middleware);
            };

            function middleware(command, event, client, next) {
                var eventType = command.replace(/\s/g, "_");
                if (eventType !== "ping" && eventType !== "pong") { // Shutup
                    self.log.debug("Event:", eventType);
                }

                let normalizedEvent = self.normalizeEvent(eventType, event);
                self.emit(normalizedEvent.type, normalizedEvent.event);
                next();
            }
        }
        self.irc.use(EventMiddleware());

        // Enable debug (raw) output
        if (self.settings.debug) {
            self.irc.on("raw", (event) => {
                var direction = event.from_server ? ">" : "<";
                self.log.debug(`S ${direction} C | ${event.line}`);
            });
        }

        // Invalid nicks
        self.on("nick_invalid", (event) => {
            if (!this.registered) {
                self.log.error(`The nick ${event.nick} is invalid: ${event.reason}`);
                // TODO: Have a way to reconnect
                self.disconnect();
            }
        });

        self.on("registered", (event) => {
            this.registered = true;
        });
    }

    connect() {
        this.log.info("Connecting");
        this.registered = false;
        this.irc.connect(this.settings);

        setTimeout(() => {
            if (!this.irc.connected) {
                this.log.error("Could not connect");
                this.disconnect();
                this.emit("error_connecting");
            }
        }, 10 * 1000);
    }

    disconnect() {
        this.registered = false;
        this.log.info("Disconnecting");
        this.irc.quit();
    }

    normalizeEvent(type, event) {
        switch(type) {
            case "join":
                // our own join event doesn't have a mode key
                if (typeof event.mode === "undefined") {
                    event.mode = [];
                }
                break;

            case "mode":
                // event.type is undefined
                event.type = "mode";
                break;

            case "topicsetby":
                // `topicsetby` uses 'when' for time, and `topic` uses 'time' for time
                event.time = event.when;
                break;
        }

        return {type, event};
    }
}

module.exports = Connection;
