const CouchEvents = require(__serverRoot + "CouchEvents");
const log = require(__serverRoot + "Logger")("ClientManager");
const Client = require(__serverRoot + "server/Client");

var ClientManager = function() {
    var clients = {},
        id = 0;

    // Events
    CouchEvents.on("ws.connect", onClientConnect);
    CouchEvents.on("couch.server_shutdown", () => broadcast("server_shutdown"));

    function onClientConnect(ws) {
        id++;
        var client = new Client(id, ws);
        clients[id] = client;
        //log.debug(`Client ${id} connected.`);

        CouchEvents.emit("client.connected", client);

        client.on("ws.disconnected", () => {
            delete clients[client.clientId];
            CouchEvents.emit("client.disconnected", client);
        });
    }

    // Methods for clients
    function sendToClient(clientId, event, data) {
        clients[clientId].send(event, data);
    }

    function broadcast(event, data) {
        log.debug("Broadcasting", event, data);
        for (let clientId in clients) {
            sendToClient(clientId, event, data);
        }
    }

    function getClient(clientId) {
        return clients[clientId];
    }

    // "API"
    return {
        sendToClient: sendToClient,
        broadcast: broadcast,
        getClient: getClient,
    };
};


module.exports = ClientManager;

