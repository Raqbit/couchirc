const EventEmitter = require("events").EventEmitter;
const Logger = require(__serverRoot + "Logger");
const CouchEvents = require(__serverRoot + "CouchEvents");

class Client extends EventEmitter {
    constructor(clientId, websocket) {
        super();
        this.clientId = clientId;
        this.ws = websocket;
        this.log = Logger("Client " + clientId);
        this.ws.client = this;

        this._init();
    }

    _init() {
        this.log.debug("Connected");

        this.ws.on("message", (msg) => {
            msg = JSON.parse(msg);
            if (msg.event !== "ping" && msg.event !== "pong") { // I get it - you are pinging - but shut up
                this.log.debug("RECV:" , msg);
            }
            this.emit(msg.event, msg.data);

            CouchEvents.emit("ws." + msg.event, {client: this, data: msg.data});
        });

        this.ws.on("close", () => {
            this.emit("ws.disconnected");
            clearInterval(pingInterval); // Stop pinging - we dead jim!
            this.log.debug("Disconnected");
        });

        this.on("ping", () => {
            this.send("pong");
        });

        // Pingtimeout
        var pingTimeout;
        var pingInterval = setInterval(() => {
            this.send("ping");

            pingTimeout = setTimeout(() => {
                this.log.debug("Ping timeout");
                this.emit("ws.timeout");
                this.kill();
            }, 2 * 1000); // Timeout after 2 seconds
        }, 20 * 1000); // Ping every 20 seconds

        this.on("pong", () => {
            clearTimeout(pingTimeout);
        });
    }

    get connected() {
        return this.ws.readyState === this.ws.OPEN;
    }

    send(event, data) {
        if (! this.connected) {
            this.log.debug("Tried to send data to closed ws", event, data);
            return;
        }
        data = data || {};
        var msg = JSON.stringify({event: event, data: data});
        if (event !== "ping" && event !== "pong") { // I get it - you are pinging - but shut up
            this.log.debug("SEND:", msg);
        }
        this.ws.send(msg);
    }

    kill() {
        this.ws.close();
    }
}


module.exports = Client;

