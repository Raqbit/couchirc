var showDebug = false;

class Logger {
    constructor(identifier) {
        this.identifier = identifier;
    }

    info() {
        console.log(`[${(new Date()).toLocaleTimeString()}] [INFO ] [${this.identifier}]`, ...arguments);
    }

    error() {
        console.error(`[${(new Date()).toLocaleTimeString()}] [ERROR] [${this.identifier}]`, ...arguments);
    }

    debug() {
        if (showDebug) {
            console.log(`[${(new Date()).toLocaleTimeString()}] [DEBUG] [${this.identifier}]`, ...arguments);
        }
    }

    setShowDebug(value) {
        showDebug = value;
    }
}

function newLogger(id) {
    return new Logger(id);
}
module.exports = newLogger;
