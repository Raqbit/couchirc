"use strict";

const express = require("express");
const expressWs = require("express-ws");
const CouchEvents = require(__serverRoot + "CouchEvents");
const Config = require(__serverRoot + "Config").config; // Assuming it is loaded already at this point

const log = require(__serverRoot + "Logger")("WebServer");

var WebServer = function () {
    // Setup
    var app = express();
    expressWs(app);

    // Static content
    app.use(express.static(__clientRoot));
    app.use("/bower", express.static(__root + "bower_components/"));

    // Middleware for logging
    app.use(function (req, res, next) {
        log.debug("Request:", req.method + " " + req.originalUrl);
        return next();
    });

    // Websocket
    app.ws("/", function(ws, req) {
        CouchEvents.emit("ws.connect", ws);
    });

    // Start the server!
    app.listen(Config.bind_port);
    log.info("Listening on port: " + Config.bind_port);

    return this;
};

module.exports = WebServer;
