"use strict";
/* eslint-env node */
// eslint-disable-next-line
/* global __serverRoot __clientRoot __root COUCH_VERSION DEVMODE*/

// Default to production
process.env.NODE_ENV = process.env.NODE_ENV || "production";

const path = require("path");

// Check that npm dependancies have been installed
var error = false;
try {
    require("irc-framework");
}
catch (err) {
    console.error("Please run `npm install` to install required dependancies.");
    error = true;
}

if (error) {
    process.exit();
}

// End of startup checks

process.chdir(__dirname);
global.__root = path.resolve(__dirname) + path.sep;
global.__serverRoot = path.resolve(__dirname, "server") + path.sep;
global.__clientRoot = path.resolve(__dirname, "client") + path.sep;
global.COUCH_VERSION = require(__root + "package.json").version;
global.DEVMODE = process.env.NODE_ENV !== "production";

const CouchEvents = require(__serverRoot + "CouchEvents");


// TODO many things (mostly args stuff/commands), but for now, this just start it
// eslint-disable-next-line
const Couch = require(__serverRoot + "Couch");



// Detect Ctrl+C on windows
// Credit to http://stackoverflow.com/a/14861513
if (process.platform === "win32") {
    var rl = require("readline").createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.on("SIGINT", function () {
        process.emit("SIGINT");
    });
}

process.on("SIGINT", function () {
    console.log("Shutting down...");
    CouchEvents.emit("couch.shutdown");

    //graceful shutdown
    console.log("Done.");
    process.exit();
});
